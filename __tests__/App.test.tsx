import { it } from '@jest/globals';
import { render } from '@testing-library/react-native';
import React from 'react';
import 'react-native';
import { App } from '../App';

const setup = () => {
  return render(<App />);
};

describe('App component', () => {
  it('renders without crashing', () => {
    const component = setup().toJSON();
    expect(component).toMatchSnapshot();
  });
});
