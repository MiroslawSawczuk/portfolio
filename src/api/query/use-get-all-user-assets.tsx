import { getHttpClient } from '@api';
import { keepPreviousData, useQuery } from '@tanstack/react-query';
import { UserAsset } from '@types';

export const useGetAllUserAssets = () =>
  useQuery({
    queryKey: ['get-all-user-assets'],
    queryFn: () => getAllUserAssets(),
    placeholderData: keepPreviousData,
  });

export const getAllUserAssets = async (): Promise<UserAsset[]> =>
  await getHttpClient().get<unknown, UserAsset[]>('Asset/GetAllUserAssets');
