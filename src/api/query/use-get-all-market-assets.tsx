import { getHttpClient } from '@api';
import { keepPreviousData, useQuery } from '@tanstack/react-query';
import { MarketAsset } from '@types';

export const useGetAllMarketAssets = () =>
  useQuery({
    queryKey: ['get-all-market-assets'],
    queryFn: () => getAllMarketAssets(),
    placeholderData: keepPreviousData,
  });

export const getAllMarketAssets = async (): Promise<MarketAsset[]> =>
  await getHttpClient().get<unknown, MarketAsset[]>(
    'Market/GetAllMarketAssets',
  );
