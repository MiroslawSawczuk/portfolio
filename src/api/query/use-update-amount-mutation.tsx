import { getHttpClient } from '@api';
import { useMutation, useQueryClient } from '@tanstack/react-query';
import { UserAsset } from '@types';

type UpdateAmount = {
  userId: number;
  symbol: string;
  amount: number;
};

export const useUpdateAmountMutation = () => {
  const queryClient = useQueryClient();

  return useMutation({
    mutationFn: (props: UpdateAmount) => updateAmount(props),
    onSuccess: () => {
      queryClient.invalidateQueries({ queryKey: ['get-all-user-assets'] });
    },
  });
};

export const updateAmount = async (props: UpdateAmount): Promise<UserAsset> =>
  await getHttpClient().put<unknown, UserAsset>('Asset/UpdateAmount', props);
