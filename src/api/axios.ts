import axios, { AxiosInstance } from 'axios';

const BASE_URL = 'https://portfolio-bff.azurewebsites.net/api/';

let instance: AxiosInstance | undefined;

export const initHttpClient = () => {
  instance = axios.create({
    baseURL: BASE_URL,
    timeout: 60000,
  });

  instance.interceptors.response.use(response => response.data);

  if (!instance) {
    return;
  }

  //TODO add auth interceptor and handling accessToken if needed
};

export const getHttpClient = () => {
  if (!instance) {
    throw new Error("HttpClient isn't initialized");
  }
  return instance;
};
