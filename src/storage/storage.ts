import EncryptedStorage from 'react-native-encrypted-storage';

export const setItem = async (key: string, value: string) => {
  try {
    await EncryptedStorage.setItem(key, value);
  } catch (error) {
    console.log(`Error during setItem: ${key}.`, error);
  }
};
export const getItem = async (key: string) => {
  let result = '';
  try {
    result = (await EncryptedStorage.getItem(key)) || '';
  } catch (error) {
    console.log(`Error during getItem: ${key}.`, error);
  }

  return result;
};
