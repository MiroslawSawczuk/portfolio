export const formatCurrency = (value: number) =>
  new Intl.NumberFormat('en-EN', {
    style: 'currency',
    currency: 'USD',
  }).format(value);
