import { BottomStack } from '@navigation';
import { NavigationContainer } from '@react-navigation/native';
import React from 'react';

export const MainNavigator = () => {
  return (
    <NavigationContainer>
      <BottomStack />
    </NavigationContainer>
  );
};
