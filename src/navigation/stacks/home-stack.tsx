import { HOME_SCREEN_NAME, HomeStackParamList } from '@navigation';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { HomeScreen } from '@screens';
import React from 'react';

const { Navigator, Screen } = createNativeStackNavigator<HomeStackParamList>();
const screenOptions = {
  headerShown: false,
};

export const HomeStack = () => {
  return (
    <Navigator screenOptions={screenOptions}>
      <Screen name={HOME_SCREEN_NAME.HOME} component={HomeScreen} />
    </Navigator>
  );
};
