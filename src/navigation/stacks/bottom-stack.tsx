import { BottomTabs } from '@components';
import {
  BOTTOM_STACK_NAME,
  BottomStackParamList,
  HomeStack,
  PortfolioStack,
} from '@navigation';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { BottomTabBarProps } from '@react-navigation/bottom-tabs/lib/typescript/src/types';
import React from 'react';

const { Navigator, Screen } = createBottomTabNavigator<BottomStackParamList>();
const screenOptions = {
  headerShown: false,
};

export const BottomStack = () => {
  const renderTabs = (props: BottomTabBarProps) => {
    return <BottomTabs {...props} />;
  };

  return (
    <Navigator screenOptions={screenOptions} tabBar={renderTabs}>
      <Screen
        name={BOTTOM_STACK_NAME.HOME_STACK}
        component={HomeStack}
        options={{ title: 'Market' }}
      />
      <Screen
        name={BOTTOM_STACK_NAME.PORTFOLIO_STACK}
        component={PortfolioStack}
        options={{ title: 'Portfolio' }}
      />
    </Navigator>
  );
};
