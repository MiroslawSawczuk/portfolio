import { PORTFOLIO_SCREEN_NAME, PortfolioStackParamList } from '@navigation';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { EditAssetScreen, PortfolioScreen } from '@screens';
import React from 'react';

const { Navigator, Screen } =
  createNativeStackNavigator<PortfolioStackParamList>();

export const PortfolioStack = () => {
  return (
    <Navigator>
      <Screen
        name={PORTFOLIO_SCREEN_NAME.PORTFOLIO}
        component={PortfolioScreen}
        options={{
          title: 'Portfolio',
        }}
      />
      <Screen
        name={PORTFOLIO_SCREEN_NAME.EDIT_ASSET}
        options={{
          headerShown: false,
          presentation: 'modal',
        }}
        component={EditAssetScreen}
      />
    </Navigator>
  );
};
