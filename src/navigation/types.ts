import { NavigatorScreenParams, RouteProp } from '@react-navigation/native';
import { NativeStackScreenProps } from '@react-navigation/native-stack';

//STACK NAMES
export enum BOTTOM_STACK_NAME {
  HOME_STACK = 'HOME_STACK',
  PORTFOLIO_STACK = 'PORTFOLIO_STACK',
}

//SCREEN NAMES
export enum HOME_SCREEN_NAME {
  HOME = 'HOME',
}

export enum PORTFOLIO_SCREEN_NAME {
  PORTFOLIO = 'PORTFOLIO',
  EDIT_ASSET = 'EDIT_ASSET',
}

//SCREEN PARAM LISTS
export type HomeStackParamList = {
  [HOME_SCREEN_NAME.HOME]: undefined;
};

export type PortfolioStackParamList = {
  [PORTFOLIO_SCREEN_NAME.PORTFOLIO]: undefined;
  [PORTFOLIO_SCREEN_NAME.EDIT_ASSET]: { symbol: string; amount: number };
};

//STACK PARAM LISTS
export type BottomStackParamList = {
  [BOTTOM_STACK_NAME.HOME_STACK]: NavigatorScreenParams<HomeStackParamList>;
  [BOTTOM_STACK_NAME.PORTFOLIO_STACK]: NavigatorScreenParams<PortfolioStackParamList>;
};

//NAVIGATION AND ROUTE PROPS
export type HomeScreenNavigationProp = NativeStackScreenProps<
  HomeStackParamList,
  HOME_SCREEN_NAME.HOME
>['navigation'];
export type HomeRouteProps = RouteProp<
  HomeStackParamList,
  HOME_SCREEN_NAME.HOME
>;

export type PortfolioScreenNavigationProp = NativeStackScreenProps<
  PortfolioStackParamList,
  PORTFOLIO_SCREEN_NAME.PORTFOLIO
>['navigation'];
export type PortfolioRouteProps = RouteProp<
  PortfolioStackParamList,
  PORTFOLIO_SCREEN_NAME.PORTFOLIO
>;

export type EditAssetScreenNavigationProp = NativeStackScreenProps<
  PortfolioStackParamList,
  PORTFOLIO_SCREEN_NAME.EDIT_ASSET
>['navigation'];
export type EditAssetRouteProps = RouteProp<
  PortfolioStackParamList,
  PORTFOLIO_SCREEN_NAME.EDIT_ASSET
>;
