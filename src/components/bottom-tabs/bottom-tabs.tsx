import { BottomTabBarProps } from '@react-navigation/bottom-tabs/lib/typescript/src/types';
import React from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import { useBottomTabsStyles } from 'src/components/bottom-tabs/bottom-tabs.styles';

export const BottomTabs = ({
  state,
  descriptors,
  navigation,
}: BottomTabBarProps) => {
  const styles = useBottomTabsStyles();

  return (
    <View style={styles.container}>
      {state.routes.map((route, index) => {
        const { options } = descriptors[route.key];
        const label = options.title;

        const isFocused = state.index === index;

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
            canPreventDefault: true,
          });

          if (!isFocused && !event.defaultPrevented) {
            navigation.navigate(route.name, route.params);
          }
        };

        return (
          <TouchableOpacity key={index} onPress={onPress} style={styles.tab}>
            <Text style={[styles.label, isFocused && styles.focused]}>
              {label}
            </Text>
          </TouchableOpacity>
        );
      })}
    </View>
  );
};
