import { StyleSheet } from 'react-native';
import { useTheme } from 'react-native-paper';

export const useBottomTabsStyles = () => {
  const { colors } = useTheme();

  return StyleSheet.create({
    container: {
      flexDirection: 'row',
      height: 50,
      justifyContent: 'center',
      alignItems: 'center',
    },
    tab: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    },
    label: {
      color: '#222',
    },
    focused: {
      color: colors.primary,
      fontWeight: '700',
    },
  });
};
