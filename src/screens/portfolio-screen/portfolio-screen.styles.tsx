import { StyleSheet } from 'react-native';

export const usePortfolioScreenStyles = () => {
  return StyleSheet.create({
    container: {
      flex: 1,
    },
    title: {
      margin: 10,
      fontSize: 25,
    },
    list: {
      paddingHorizontal: 10,
    },
  });
};
