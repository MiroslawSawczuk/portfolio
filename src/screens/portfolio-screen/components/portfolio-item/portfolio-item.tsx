import { UserAsset } from '@types';
import React from 'react';
import { TouchableOpacity, View } from 'react-native';
import { Icon, Text } from 'react-native-paper';
import { usePortfolioItemStyles } from 'src/screens/portfolio-screen/components/portfolio-item/portfolio-item.styles';
import { formatCurrency } from 'src/utils';

type Props = {
  asset: UserAsset;
  onEditItem: (symbol: string, amount: number) => void;
};
export const PortfolioItem = ({ asset, onEditItem }: Props) => {
  const styles = usePortfolioItemStyles();

  return (
    <View style={styles.container}>
      <Text style={styles.cell2}>{asset.symbol.toUpperCase()}</Text>
      <Text style={styles.cell3}>
        {formatCurrency(Number(asset.currentPrice))}
      </Text>
      <Text style={styles.cell2}>{asset.amount.toFixed(2)}</Text>
      <Text style={styles.cell3}>{formatCurrency(asset.totalValue)}</Text>
      <TouchableOpacity
        style={styles.cell1}
        onPress={() => onEditItem(asset.symbol, asset.amount)}>
        <Icon source="pencil" size={20} />
      </TouchableOpacity>
    </View>
  );
};
