import { useNavigation } from '@react-navigation/native';
import React from 'react';
import { ActivityIndicator, FlatList, View } from 'react-native';
import { Text } from 'react-native-paper';
import { useGetAllUserAssets } from 'src/api/query/use-get-all-user-assets';
import {
  PORTFOLIO_SCREEN_NAME,
  PortfolioScreenNavigationProp,
} from 'src/navigation';
import { ListHeader } from 'src/screens/portfolio-screen/components/list-header/list-header';
import { PortfolioItem } from 'src/screens/portfolio-screen/components/portfolio-item/portfolio-item';
import { usePortfolioScreenStyles } from 'src/screens/portfolio-screen/portfolio-screen.styles';
import { formatCurrency } from 'src/utils';

export const PortfolioScreen = () => {
  const styles = usePortfolioScreenStyles();
  const { data, isLoading, isError } = useGetAllUserAssets();
  const navigation = useNavigation<PortfolioScreenNavigationProp>();

  const portfolioValue =
    data?.reduce((acc, asset) => acc + asset.totalValue, 0) || 0;

  const onEditItem = (symbol: string, amount: number) =>
    navigation.navigate(PORTFOLIO_SCREEN_NAME.EDIT_ASSET, { symbol, amount });

  if (isLoading) {
    return <ActivityIndicator />;
  }
  if (!data || isError) {
    //TODO handle error state
    return null;
  }

  return (
    <View style={styles.container}>
      <Text style={styles.title}>
        Portfolio value: {formatCurrency(portfolioValue)}
      </Text>
      <FlatList
        data={data}
        renderItem={({ item }) => (
          <PortfolioItem asset={item} onEditItem={onEditItem} />
        )}
        keyExtractor={item => item.symbol}
        ListHeaderComponent={<ListHeader />}
        style={styles.list}
      />
    </View>
  );
};
