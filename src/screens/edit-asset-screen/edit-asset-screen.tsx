import { useNavigation, useRoute } from '@react-navigation/native';
import React, { useState } from 'react';
import {
  Keyboard,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import { Button, Icon, Text, TextInput } from 'react-native-paper';
import { useUpdateAmountMutation } from 'src/api/query/use-update-amount-mutation';
import {
  EditAssetRouteProps,
  EditAssetScreenNavigationProp,
} from 'src/navigation';
import { useEditAssetScreenStyles } from 'src/screens/edit-asset-screen/edit-asset-screen.styles';

export const EditAssetScreen = () => {
  const { params } = useRoute<EditAssetRouteProps>();
  const navigation = useNavigation<EditAssetScreenNavigationProp>();
  const styles = useEditAssetScreenStyles();
  const [value, setValue] = useState(params.amount.toFixed(2));
  const { mutateAsync: updateAmount, isPending } = useUpdateAmountMutation();

  const onPressSave = async () => {
    if (value) {
      try {
        await updateAmount({
          userId: 1,
          symbol: params.symbol,
          amount: Number(value),
        });
      } catch (error) {
        //TODO add error handling
      }

      navigation.goBack();
    }
  };

  return (
    <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
      <View style={styles.container}>
        <View style={styles.titleContainer}>
          <Text style={styles.title}>Edit {params.symbol.toUpperCase()}</Text>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Icon source="close" size={20} />
          </TouchableOpacity>
        </View>
        <View style={styles.innerContainer}>
          <TextInput
            label={'Provide value'}
            value={value}
            onChangeText={text => setValue(text)}
            keyboardType="numeric"
            placeholder="0.00"
            returnKeyType="done"
          />

          <Button
            mode="contained"
            onPress={onPressSave}
            disabled={!value}
            loading={isPending}>
            Save
          </Button>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
};
