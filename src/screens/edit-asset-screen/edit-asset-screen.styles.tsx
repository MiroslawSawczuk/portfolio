import { StyleSheet } from 'react-native';

export const useEditAssetScreenStyles = () => {
  return StyleSheet.create({
    container: {
      flex: 1,
      paddingTop: 20,
      paddingHorizontal: 20,
    },
    titleContainer: {
      flexDirection: 'row',
      justifyContent: 'space-between',
    },
    title: {
      fontSize: 25,
      paddingHorizontal: 10,
    },
    innerContainer: {
      flex: 1,
      marginVertical: 20,
      justifyContent: 'space-between',
    },
  });
};
