import { StyleSheet } from 'react-native';

export const useHomeScreenStyles = () => {
  return StyleSheet.create({
    container: {
      flex: 1,
      paddingTop: 40,
    },
    title: {
      fontSize: 25,
      paddingHorizontal: 20,
      paddingVertical: 10,
    },
    list: {
      paddingHorizontal: 10,
    },
    appVersion: {
      justifyContent: 'center',
      alignItems: 'center',
    },
  });
};
