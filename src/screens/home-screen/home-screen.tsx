import { useGetAllMarketAssets } from '@api';
import React from 'react';
import { ActivityIndicator, FlatList, View } from 'react-native';
import Config from 'react-native-config';
import DeviceInfo from 'react-native-device-info';
import { Text } from 'react-native-paper';
import { AssetItem } from 'src/screens';
import { ListHeader } from 'src/screens/home-screen/components/list-header/list-header';
import { useHomeScreenStyles } from 'src/screens/home-screen/home-screen.styles';

export const HomeScreen = () => {
  const styles = useHomeScreenStyles();
  const { isLoading, data } = useGetAllMarketAssets();

  const versionNumber = DeviceInfo.getVersion();
  const buildNumber = DeviceInfo.getBuildNumber();

  if (isLoading) {
    return <ActivityIndicator />;
  }

  return (
    <View style={styles.container}>
      <View style={styles.appVersion}>
        <Text>
          version: {versionNumber} ({buildNumber})
        </Text>
        <Text>app name config : {Config.APP_NAME}</Text>
      </View>

      <Text style={styles.title}>Top 10 market caps</Text>
      <FlatList
        data={data}
        renderItem={({ item }) => <AssetItem asset={item} />}
        keyExtractor={item => item.symbol}
        ListHeaderComponent={<ListHeader />}
        style={styles.list}
      />
    </View>
  );
};
