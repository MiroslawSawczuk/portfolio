import { StyleSheet } from 'react-native';

export const useListHeaderStyles = () => {
  return StyleSheet.create({
    container: {
      flex: 1,
      flexDirection: 'row',
      height: 60,
      justifyContent: 'flex-start',
      alignItems: 'center',
    },
    cell1: {
      flex: 1,
      fontWeight: '700',
    },
    cell2: {
      flex: 2,
      fontWeight: '700',
    },
    cell3: {
      flex: 3,
      fontWeight: '700',
    },
  });
};
