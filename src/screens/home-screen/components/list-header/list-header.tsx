import React from 'react';
import { View } from 'react-native';
import { Text } from 'react-native-paper';
import { useListHeaderStyles } from 'src/screens/home-screen/components/list-header/list-header.styles';

export const ListHeader = () => {
  const styles = useListHeaderStyles();

  return (
    <View style={styles.container}>
      <Text style={styles.cell1}>Name</Text>
      <Text style={styles.cell2}>Current price</Text>
      <Text style={styles.cell3}>Chart</Text>
    </View>
  );
};
