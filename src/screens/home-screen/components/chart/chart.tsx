import React, { useState } from 'react';
import { LayoutChangeEvent, View } from 'react-native';
import { LineChart, TLineChartPoint } from 'react-native-wagmi-charts';
import { ChartPricesDict } from 'src/types';

type Props = {
  prices: ChartPricesDict;
};
export const Chart = ({ prices }: Props) => {
  const [width, setWidth] = useState(0);

  const chartData = Object.keys(prices).map(
    price =>
      ({
        timestamp: Number(price),
        value: prices[price],
      } as TLineChartPoint),
  );

  const onLayout = (event: LayoutChangeEvent) => {
    const containerWidth = event.nativeEvent.layout.width;
    setWidth(containerWidth);
  };

  if (!chartData) {
    return null;
  }

  const firstPoint = chartData?.[0].value;
  const lastPoint = chartData?.[chartData.length - 1].value;

  const isPositive = firstPoint <= lastPoint;

  return (
    <View onLayout={onLayout}>
      <LineChart.Provider data={chartData}>
        <LineChart height={50} width={width}>
          <LineChart.Path color={isPositive ? '#00a83e' : '#ff3a33'} />
        </LineChart>
      </LineChart.Provider>
    </View>
  );
};
