import { StyleSheet } from 'react-native';

export const useAssetItemStyles = () => {
  return StyleSheet.create({
    container: {
      flex: 1,
      flexDirection: 'row',
      height: 60,
      justifyContent: 'flex-start',
      alignItems: 'center',
    },
    cell1: {
      flex: 1,
    },
    cell2: {
      flex: 2,
    },
    cell3: {
      flex: 3,
    },
  });
};
