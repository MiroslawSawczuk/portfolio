import { MarketAsset } from '@types';
import React from 'react';
import { View } from 'react-native';
import { Text } from 'react-native-paper';
import { useAssetItemStyles } from 'src/screens/home-screen/components/asset-item/asset-item.styles';
import { Chart } from 'src/screens/home-screen/components/chart/chart';
import { formatCurrency } from 'src/utils';

type Props = {
  asset: MarketAsset;
};
export const AssetItem = ({ asset }: Props) => {
  const styles = useAssetItemStyles();
  const currentPrice = formatCurrency(Number(asset.currentPrice));

  return (
    <View style={styles.container}>
      <Text style={styles.cell1}>{asset.symbol.toUpperCase()}</Text>
      <Text style={styles.cell2}>{currentPrice}</Text>
      <View style={styles.cell3}>
        <Chart prices={asset.chartData} />
      </View>
    </View>
  );
};
