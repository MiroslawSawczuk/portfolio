declare module 'react-native-config' {
  export interface NativeConfig {
    APP_NAME: string;
    BUNDLE_ID: string;
  }

  export const Config: NativeConfig;
  export default Config;
}
