type Asset = {
  id: string;
  name: string;
  symbol: string;
  imageUrl: string;
  currentPrice: number;
};

export type UserAsset = Asset & {
  amount: number;
  totalValue: number;
};

export type MarketAsset = Asset & {
  chartData: ChartPricesDict;
};

export type ChartPricesDict = {
  [timestamp: string]: number;
};
