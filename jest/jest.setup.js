import React from 'react';

jest.mock('@react-navigation/native-stack', () => {
  return {
    createNativeStackNavigator: () => ({
      Navigator: () => <></>,
      Screen: () => <></>,
    }),
  };
});

jest.mock('react-native-encrypted-storage', () => {
  return {
    setItem: jest.fn(),
    getItem: jest.fn(),
  };
});

jest.mock('react-native-wagmi-charts', () => {});

jest.mock('@react-navigation/bottom-tabs', () => {
  return {
    createBottomTabNavigator: () => ({
      Navigator: () => <></>,
      Screen: () => <></>,
    }),
  };
});

jest.mock('react-native-device-info', () => {});
jest.mock('react-native-config', () => {});
