import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import React from 'react';
import { PaperProvider } from 'react-native-paper';
import { initHttpClient } from 'src/api';
import { MainNavigator } from 'src/navigation';

const queryClient = new QueryClient();

export const App = () => {
  initHttpClient();

  return (
    <QueryClientProvider client={queryClient}>
      <PaperProvider>
        <MainNavigator />
      </PaperProvider>
    </QueryClientProvider>
  );
};
